# Papers

---

## A Predictive Control Algorithm for Layer-to-layer Ink-jet 3D Printing

### Abstract

Ink-jet 3D printing is an additive manufacturing process where-in parts are built by jetting material from nozzles layer by layer, with curing between successive layers
of deposition. Typically, this additive process is operated in an open-loop manner, i.e., the number of layers and droplet patterns for each layer are determined in advance and are not modified based on measurements of current height profile
during the printing process. As a result, the uncertainties in droplet sizes, heights and locations during printing can result in undesired part geometry, such as edge shrinking, unreliable dimensions and uneven surfaces. Naturally, incorpo-
rating feedback measurements into the control algorithm will improve performance if suitable models and cost functions are constructed. In this paper, we first propose a control-oriented layer to layer height evolution model for two-material Ink-
jet 3D printing. Based on this model, a predictive control algorithm that uses measurements of the current height profile to generate control inputs for successive layers is designed. The effectiveness of the proposed algorithm is demonstrated by simulation studies.

### Submitted by

Alexander Shkoruta

---

## Control-Oriented Models for Ink-jet 3D Printing

### Abstract

This paper proposes two control-oriented models to describe the layer-to-layer height evolution of the ink-jet 3D printing process. While the process has found wide applicability, as with many industrial 3D printing systems, ink-jet 3D printers operated in open loop, even when sensor measurements are available. One of the primary reasons for this is the lack of suitable control-oriented models of the process. To address this issue, two control-oriented models are proposed in this paper, the first is based on droplet geometry superposition, while the other is based on a graph-based characterization of local flow dynamics. The superposition model ignores the flow of the deposited liquid material, while the graph-based dynamic model is able to capture this phenomenon. These two models are compared with an existing flow-based empirical model and validated with experimental results, with the graph-based dynamic model demonstrating between 5-14 % improvement in layer height prediction. Both the superposition model and the graph-based dynamic model are suitable for closed-loop control algorithm design (as they are discrete layer-to-layer state-space models). We envision that these control-oriented models will ultimately enable the design of model-based closed-loop control for high resolution ink-jet 3D printing.

### Submitted by

Alexander Shkoruta

---
## Distributed Model Predictive Control for Ink-Jet 3D Printing

### Abstract

This paper develops a closed-loop approach for ink-jet 3D printing. The control design is based on a distributed model predictive control scheme, which can handle constraints (such as droplet volume) as well as the large-scale nature of the problem. The high resolution of ink-jet 3D printing make centralized methods extremely time-consuming, thus a distributed implementation of the controller is developed. First a graph-based height evolution model that can capture the
liquid flow dynamics is proposed. Then, a scalable closed-loop control algorithm is designed based on the model using Distributed MPC, that reduces computation time significantly. The performance and efficiency of the algorithm are shown to outperform open-loop printing and closed-loop printing with existing Centralized MPC methods through simulation results.

### Submitted by

Alexander Shkoruta

---
## Heat Transfer Model and Finite Element Formulation for Simulation of Selective Laser Melting

### Abstract

A novel approach and finite element formulation for modeling the melting, consolidation, and re-solidification process that occurs in selective laser melting additive manufacturing is presented. Two state variables are introduced to track the phase (melt/solid) and the degree of consolidation (powder/fully dense). The effect of the consolidation on the absorption of the laser energy into the material as it transforms from a porous powder to a dense melt is considered. A Lagrangian finite element formulation, which solves the governing equations on the unconsolidated reference configuration is derived, which naturally considers the effect of the changing geometry as the powder melts without needing to update the simulation domain. The finite element model is implemented into a general-purpose parallel finite
element solver. Results are presented comparing to experimental results in the literature for a single laser track with good agreement. Predictions for a spiral laser pattern are also shown.

### Submitted by

Alexander Shkoruta

---
## Process Defects and In Situ Monitoring Methods in Metal Powder Bed Fusion: A Review

### Abstract

Additive manufacturing and specifically metal selective laser melting (SLM) processes are rapidly being industrialized. In order for this technology to see more widespread use as a production modality, especially in heavily regulated industries such as aerospace and medical device manufacturing, there is a need for robust process monitoring and control capabilities to be developed that reduce process variation and ensure quality. The current state of the art of such process monitoring technology is reviewed in this paper. The SLM process itself presents significant challenges as over 50 different process input variables impact the characteristics of the finished part. Understanding the impact of feed powder characteristics remains a challenge. Though many powder characterization techniques have been developed, there is a need for standardization of methods most relevant to additive manufacturing. In-process sensing technologies have primarily focused on monitoring melt pool signatures, either from a Lagrangian reference frame that follows the focal point of the laser or from a fixed Eulerian reference frame. Correlations between process measurements, process parameter settings, and quality metrics to date have been primarily qualitative. Some simple, first-generation process control strategies have also been demonstrated based on these measures. There remains a need for connecting process measurements to process models to enable robust model-based control.

### Submitted by

Alexander Shkoruta

---

