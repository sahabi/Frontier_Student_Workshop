# Papers

---

## Near-Optimal Machine Teaching via Explanatory Teaching Sets

### Abstract

Modern applications of machine teaching for humans often involve domain-specific, nontrivial target hypothesis classes. To facilitate understanding of the target hypothesis, it is crucial for the teaching algorithm to use examples which are interpretable to the human learner. In this paper, we propose NOTES, a principled framework for constructing interpretable teaching sets, utilizing explanations to accelerate the teaching process. Our algorithm is built upon a natural stochastic model of learners and a novel submodular surrogate objective function which greedily
selects interpretable teaching examples. We prove that NOTES is competitive with the
optimal explanation-based teaching strategy. We further instantiate NOTES with a specific hypothesis class, which can be viewed as an interpretable approximation of any hypothesis class, allowing us to handle complex hypothesis in practice. We demonstrate the effectiveness of NOTES on several image classification tasks, for both simulated and real human learners. Our experimental results suggest that by leveraging explanations, one can significantly speed up teaching.

### Submitted by

Yuxin Chen

---

## Passivity-based Iterative Learning Control Design for Selective Laser Melting

### Abstract

Selective laser melting (SLM) is an additive manufacturing process that creates 3D parts through layer by layer melting and fusion of a metal powder bed. Although a
number of finite element models (FEM) have been developed that describe the coupled and complex physics associated with this process, they are typically not suitable for control algorithm design. In this manuscript, a control oriented reduced order model (ROM) to adequately capture these temperature dynamics is proposed and validated against high fidelity FEM simulations. Further, since the laser paths are often repetitive (or consist of repeating sub-trajectories), iterative learning control (ILC) algorithms can be used to obtain suitable laser power profiles to deliver desired temperature field profiles. However, the process is inherently multiple input single output (MISO), therefore, a suitable output is constructed in such a way so as to the make the system passive. A passivity-based ILC law is then designed to drive this synthesized output to a desired profile and a convergence criterion for this law derived. The proposed ILC update law is implemented on both models and the results are compared for a set of candidate laser paths. Finally, the ILC update law is implemented on the high-fidelity FEM to melt a ring geometry to demonstrate the capability of the ILC algorithm to generate optimal laser power profiles for creating complicated geometries on large powder beds. 

### Submitted by

Alexander J Nettekoven

---
