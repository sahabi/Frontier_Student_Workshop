# Papers

---

## Near-Optimal Machine Teaching via Explanatory Teaching Sets

### Abstract

Modern applications of machine teaching for
humans often involve domain-specific, nontrivial 
target hypothesis classes. To facilitate
understanding of the target hypothesis, it
is crucial for the teaching algorithm to use
examples which are interpretable to the human
learner. In this paper, we propose NOTES,
a principled framework for constructing
interpretable teaching sets, utilizing 
explanations to accelerate the teaching process. Our
algorithm is built upon a natural stochastic
model of learners and a novel submodular
surrogate objective function which greedily
selects interpretable teaching examples. We
prove that NOTES is competitive with the
optimal explanation-based teaching strategy.
We further instantiate NOTES with a specific
hypothesis class, which can be viewed as an
interpretable approximation of any hypothesis 
class, allowing us to handle complex
hypothesis in practice. We demonstrate the
effectiveness of NOTES on several image
classification tasks, for both simulated and
real human learners. Our experimental results
suggest that by leveraging explanations, one
can significantly speed up teaching.

### Participant/Author

Yuxin Chen

---

## Submodular Surrogates for Value of Information

### Abstract

How should we gather information to make effective
decisions? A classical answer to this fundamental problem
is given by the decision-theoretic value of information.
Unfortunately, optimizing this objective is intractable, and
myopic (greedy) approximations are known to perform
poorly. In this paper, we introduce DIRECT , an efficient yet
near-optimal algorithm for nonmyopically optimizing value
of information. Crucially, DIRECT uses a novel surrogate
objective that is: (1) aligned with the value of information
problem (2) efficient to evaluate and (3) adaptive submodular. 
This latter property enables us to utilize an efficient
greedy optimization while providing strong approximation
guarantees. We demonstrate the utility of our approach on
four diverse case-studies: touch-based robotic localization,
comparison-based preference learning, wild-life conservation 
management, and preference elicitation in behavioral
economics. In the first application, we demonstrate DIRECT
in closed-loop on an actual robotic platform.

### Submitted by

Yuxin Chen

---

## Syntax-Guided Synthesis

### Abstract

The classical formulation of the program-synthesis
problem is to find a program that meets a correctness specifica-
tion given as a logical formula. Recent work on program synthesis
and program optimization illustrates many potential benefits
of allowing the user to supplement the logical specification
with a syntactic template that constrains the space of allowed
implementations. Our goal is to identify the core computational
problem common to these proposals in a logical framework. The
input to the syntax-guided synthesis problem (SyGuS) consists
of a background theory, a semantic correctness specification
for the desired program given by a logical formula, and a
syntactic set of candidate implementations given by a grammar.
The computational problem then is to find an implementation
from the set of candidate expressions so that it satisfies the
specification in the given theory. We describe three different
instantiations of the counter-example-guided-inductive-synthesis
(CEGIS) strategy for solving the synthesis problem, report on
prototype implementations, and present experimental results on
an initial set of benchmarks.

### Submitted by

Yuepeng Wang

---


